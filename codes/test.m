
 % Conception asservissement
 % Jcano 2016 

clear allclear;
den = [1 2 5];
num = [1 4];
H = tf(num,den);
K = 1000*H;
L = 13.2*H;
figure(2);
subplot(2,2,1);
title('Lieu des racines');
rlocus(H);
subplot(2,2,2);
title('Reponse indicielle A');
step(H/(1+H));
subplot(2,2,3);
title('Reponse indicielle B');
step(K/(1+K));
subplot(2,2,4);
title('Reponse indicielle C');
step(L/(L+1));
