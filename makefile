clean: 
	rm -f *.aux *.bbl *.blg *.lof *.log *.lot *.out *.toc 

build:
	pdflatex main
	bibtex main
	pdflatex main
	pdflatex main

publish: 
	make clean
	make build 
	make clean
	evince main.pdf

